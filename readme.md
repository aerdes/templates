# Templates

Templates is an application for quickly sending Slack messages using templates.

It is based on the [Laravel framework](https://laravel.com) and it is is open-source software licensed [MIT license](https://opensource.org/licenses/MIT).

## Contributing

We are appreciating contributions to this application. Check out the [repository](https://bitbucket.org/aerdes/templates/) and feel free to make pull requests.

## Security Vulnerabilities

If you discover a security vulnerability within the Templates application, please send an e-mail 
[templates@aerdes.com](mailto:templates@aerdes.com). All security vulnerabilities will be promptly addressed.

