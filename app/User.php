<?php

namespace App;

use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class User extends Model
{

    use HasCompositePrimaryKey;

    protected $primaryKey = ['user_id', 'team_id'];
    protected $fillable = ['user_id', 'team_id'];

    public function getAccessTokenAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function setAccessTokenAttribute($value)
    {
        $this->attributes['access_token'] = Crypt::encryptString($value);
    }

}