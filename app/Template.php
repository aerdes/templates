<?php

namespace App;

use App\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

    use HasCompositePrimaryKey;

    protected $primaryKey = ['user_id', 'team_id', 'name'];

}
