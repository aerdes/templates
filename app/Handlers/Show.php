<?php

namespace App\Handlers;

use App\Template;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\AttachmentField;

class Show extends SignatureHandler
{

    protected $signature = '* show {name? :  The name of the template you want to see}';
    protected $description = 'List all templates or provide information about a single template';

    public function handle(Request $request): Response
    {
        if ($command = $this->getArgument('name')) {
            return $this->show_template($request, $this->getArgument('name'));
        } else {
            return $this->show_all_templates($request);
        }
    }

    protected function show_template(Request $request, string $name): Response
    {
        // Get the template
        $template = Template::find([
            'user_id' => $request->userId,
            'team_id' => $request->teamId,
            'name' => $name
        ]);
        // Check if the template actually exists
        if (!$template) {
            return $this->respondToSlack('The template *'.$name.'* does not exist. To add it please type `/'.$request->command.' add <name> <text>`.');
        }
        // Map the template to an attachment field array
        $fields = array(AttachmentField::create($name, $template->text));
        // Send the response
        return $this->respondToSlack('The requested template is displayed below. To edit this template please type `/'.$request->command.' edit '.$name.' <text>`.')
            ->withAttachment(
                Attachment::create()->setFields($fields)
            );
    }

    protected function show_all_templates(Request $request): Response
    {
        // Get all templates
        $templates = Template::where('user_id', $request->userId)->where('team_id', $request->teamId)->orderBy('name')->get();
        // Check if the templates actually exists
        if (!$templates->count()) {
            return $this->respondToSlack('No templates are defined yet. To add a new template please type `/'.$request->command.' add <name> <text>`.');
        }
        // Map the templates to an attachment field array
        $fields = $templates->map(function (Template $template) {
            return AttachmentField::create($template->name, $template->text);
        })->all();
        // Send the response
        return $this->respondToSlack('The available templates are listed below. To add a new template please type `/'.$request->command.' add <name> <text>`.')
            ->withAttachment(
                Attachment::create()->setFields($fields)
            );
    }

}