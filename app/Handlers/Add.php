<?php

namespace App\Handlers;

use App\Template;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\AttachmentField;

class Add extends SignatureHandler
{

    protected $signature = '* add {name : The name for the template} {text?* : The text for the template (put it in straight quotes)}';
    protected $description = 'Add a new template';

    public function handle(Request $request): Response
    {
        // Validate the inputs
        $this->input->setArgument('text', is_array($this->getArgument('text')) ? implode(' ', $this->getArgument('text')) : $this->getArgument('text'));
        $validator = Validator::make($this->getArguments(), [
            'name' => ['required',
                       'string',
                       'max:100',
                       Rule::unique('templates')->where(function ($query) use ($request) {
                           return $query->where('user_id', $request->userId)->where('team_id', $request->teamId);
                       })],
            'text' => 'required|string|max:65535',
        ]);
        // Send an error if needed
        if ($validator->fails()) {
            return $this->respondToSlack('The template *'.$this->getArgument('name').'* could not be added. The following error(s) occurred.')
                ->withAttachment(
                    Attachment::create()
                        ->setColor('danger')
                        ->setText(implode("\n- ", $validator->errors()->all()))
                );
        }
        // Create the template
        $template = new Template;
        $template->user_id = $request->userId;
        $template->team_id = $request->teamId;
        $template->name = $this->getArgument('name');
        $template->text = $this->getArgument('text');
        $template->save();
        // Map the template to an attachment field array
        $fields = array(AttachmentField::create($template->name, $template->text));
        // Send the response
        return $this->respondToSlack('The template *'.$this->getArgument('name').'* was successfully added. Enjoy using it!')
            ->withAttachment(
                Attachment::create()->setFields($fields)
            );
    }

}