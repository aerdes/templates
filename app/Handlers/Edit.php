<?php

namespace App\Handlers;

use App\Template;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\AttachmentField;

class Edit extends SignatureHandler
{

    protected $signature = '* edit {name : The name of the template} {text?* : The text for the template (put it in straight quotes)}';
    protected $description = 'Edit an existing template';

    public function handle(Request $request): Response
    {
        // Validate the inputs
        $this->input->setArgument('text', is_array($this->getArgument('text')) ? implode(' ', $this->getArgument('text')) : $this->getArgument('text'));
        $validator = Validator::make($this->getArguments(), [
            'name' => [Rule::exists('templates')->where(function ($query) use ($request) {
                            $query->where('user_id', $request->userId)->where('team_id', $request->teamId);
                       })],
            'text' => 'required|string|max:65535',
        ]);
        // Send an error if needed
        if ($validator->fails()) {
            return $this->respondToSlack('The template *'.$this->getArgument('name').'* with text _'.$this->getArgument('text').
                '_ could not be edit. The following error(s) occurred.')
                ->withAttachment(
                    Attachment::create()
                        ->setColor('danger')
                        ->setText(implode("\n- ", $validator->errors()->all()))
                );
        }
        // Edit the template
        $template = Template::find([
            'user_id' => $request->userId,
            'team_id' => $request->teamId,
            'name' => $this->getArgument('name')
        ]);
        $template->text = $this->getArgument('text');
        $template->save();
        // Map the template to an attachment field array
        $fields = array(AttachmentField::create($template->name, $template->text));
        // Send the response
        return $this->respondToSlack('The template *'.$this->getArgument('name').'* was successfully edited. Have fun using the template!')
            ->withAttachment(
                Attachment::create()->setFields($fields)
            );
    }

}