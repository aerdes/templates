<?php

namespace App\Handlers;

use App\Template;
use App\User;
use App\Notifications\Message;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Attachment;

class Send extends SignatureHandler
{

    protected $signature = '* send {name : The name of the template you want to send} {recipients?* : The recipients of the template (can be users or channels, if not specified the template will be send to the current user or channel)}';
    protected $description = 'Send a template';

    public function handle(Request $request): Response
    {
        // First check if we have permissions
        $user = User::find(['user_id' => $request->userId, 'team_id' => $request->teamId]);
        if (!$user) {
            return $this->respondToSlack('In order to send templates you first need to give authorization to the app. <https://slack.com/oauth/v2/authorize?client_id='.config('services.slack.client.id').'&user_scope=chat:write|Click here> to do so.');
        }
        // Validate the inputs
        $validator = Validator::make($this->getArguments(), [
            'name' => [Rule::exists('templates')->where(function ($query) use ($request) {
                $query->where('user_id', $request->userId)->where('team_id', $request->teamId);
            })],
        ]);
        // Send an error if needed
        if ($validator->fails()) {
            return $this->respondToSlack('The template *'.$this->getArgument('name').'* does not exist and therefore cannot be send. To show a list of available templates please type `/'.$request->command.' show`.');
        }
        // Prepare messages
        $notifiable = Notification::route('slack', $user->access_token);
        $recipients = count($this->getArgument('recipients')) ? $this->getArgument('recipients') : [$request->channelId];
        $template = Template::find([
            'user_id' => $request->userId,
            'team_id' => $request->teamId,
            'name' => $this->getArgument('name')
        ]);
        $errors = [];
        // Send the messages
        foreach ($recipients as $recipient) {
            $message = new Message((new SlackMessage())->to($recipient)->content($template->text));
            try {
                $notifiable->notify($message);
            } catch (Exception $e) {
                if ($e->getMessage() == 'missing_scope' or $e->getMessage() == 'token_revoked') {
                    return $this->respondToSlack('In order to send templates you first need to give authorization to the app. <https://slack.com/oauth/v2/authorize?client_id='.config('services.slack.client.id').'&user_scope=chat:write|Click here> to do so.');
                } elseif ($e->getMessage() == 'channel_not_found') {
                    $errors[] = 'The recipient *'.$recipient.'* does not exist.';
                } else {
                    $errors[] = 'We received the error code '.$e->getMessage().' from Slack. Maybe there is a problem with your configuration?';
                }
            }
        }
        // Show errors
        if ($errors) {
            return $this->respondToSlack('Sending the template *'.$this->getArgument('name').'* did not work (partly). The following error(s) occurred.')
                ->withAttachment(
                    Attachment::create()
                        ->setColor('danger')
                        ->setText(implode("\n- ", $errors))
                );
        }
        // Send the response (empty if posted to current channel)
        // TODO: Find out why channel recipients result in unknown-channel name in the response
        if ($recipients==[$request->channelId]) {
            response(null, 200); die;
        }
        return $this->respondToSlack('You successfully send the template *'.$template->name.'* to <'. implode("> <", $recipients).'>');
    }

}