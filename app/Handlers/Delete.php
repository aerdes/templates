<?php

namespace App\Handlers;

use App\Template;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;

class Delete extends SignatureHandler
{

    protected $signature = '* delete {name : The name of the template}';
    protected $description = 'Delete a template';

    public function handle(Request $request): Response
    {
        // Validate the inputs
        $validator = Validator::make($this->getArguments(), [
            'name' => [Rule::exists('templates')->where(function ($query) use ($request) {
                            $query->where('user_id', $request->userId)->where('team_id', $request->teamId);
                       })],
        ]);
        // Send an error if needed
        if ($validator->fails()) {
            return $this->respondToSlack('The template *'.$this->getArgument('name').'* does not exist and therefore cannot be deleted.');
        }
        // Delete the template
        $template = Template::find([
            'user_id' => $request->userId,
            'team_id' => $request->teamId,
            'name' => $this->getArgument('name')
        ]);
        $template->delete();
        // Send the response
        return $this->respondToSlack('The template *'.$this->getArgument('name').'* was successfully deleted.');
    }

}