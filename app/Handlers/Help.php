<?php

namespace App\Handlers;

use Illuminate\Support\Str;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Illuminate\Support\Collection;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\AttachmentField;
use Spatie\SlashCommand\HandlesSlashCommand;
use Spatie\SlashCommand\Handlers\SignatureHandler;
use Spatie\SlashCommand\Handlers\SignatureParts;

class Help extends SignatureHandler
{

    protected $signature = '* help {command? : The command you want information about}';
    protected $description = 'List all commands or provide information about a single command';

    /**
     * Handle the given request.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return \Spatie\SlashCommand\Response
     * @throws \Spatie\SlashCommand\Exceptions\FieldCannotBeAdded
     */
    public function handle(Request $request): Response
    {
        $handlers = $this->findAvailableHandlers();

        if ($command = $this->getArgument('command')) {
            return $this->displayHelpForCommand($handlers, $command);
        }

        return $this->displayListOfAllCommands($handlers, $request->command);
    }

    /**
     * Find all handlers that are available for the current SlashCommand
     * and have a signature.
     *
     * @return Collection|SignatureHandler[]
     */
    protected function findAvailableHandlers(): Collection
    {
        return collect(config('laravel-slack-slash-command.handlers'))
            ->map(function (string $handlerClassName) {
                return new $handlerClassName($this->request);
            })
            ->filter(function (HandlesSlashCommand $handler) {
                return $handler instanceof SignatureHandler;
            })
            ->filter(function (SignatureHandler $handler) {
                $signatureParts = new SignatureParts($handler->getSignature());

                return Str::is($signatureParts->getSlashCommandName(), $this->request->command);
            });
    }

    /**
     * Show the help information for a single SignatureHandler.
     *
     * @param  Collection $handlers
     * @param  string $command
     * @return Response
     * @throws \Spatie\SlashCommand\Exceptions\FieldCannotBeAdded
     */
    protected function displayHelpForCommand(Collection $handlers, string $command): Response
    {
        $helpRequest = clone $this->request;

        $helpRequest->text = $command;

        $handler = $handlers->filter(function (HandlesSlashCommand $handler) use ($helpRequest) {
            return $handler->canHandle($helpRequest);
        })->first();

        if (!$handler) {
            return $this->respondToSlack('The command *'.$command.'* does not exist. I cannot help you unfortunately. Sorry!');
        }

        $field = AttachmentField::create($handler->getFullCommand(), $handler->getHelpDescription());

        return $this->respondToSlack('Here is detailed information about the command *'.$command.'*.')
            ->withAttachment(
                Attachment::create()->addField($field)
            );
    }

    /**
     * Show a list of all available handlers.
     *
     * @param  Collection $handlers
     * @param  string $command
     * @return Response
     */
    protected function displayListOfAllCommands(Collection $handlers, string $command): Response
    {
        $fields = $handlers
            ->sort(function (SignatureHandler $handlerA, SignatureHandler $handlerB) {
                return strcmp($handlerA->getFullCommand(), $handlerB->getFullCommand());
            })
            ->map(function (SignatureHandler $handler) {
                return AttachmentField::create($handler->getFullCommand(), $handler->getDescription());
            })
            ->all();
        return $this->respondToSlack('The available commands are listed below. For more detailed information about a command please type `/'.$command.' help <command>`:')
            ->withAttachment(
                Attachment::create()->setFields($fields)
            );
    }

}