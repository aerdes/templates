<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\RedirectRequest;
use GuzzleHttp\Client;

class RedirectController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \App\Http\Requests\RedirectRequest $request
     * @return \Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __invoke(RedirectRequest $request)
    {
        $client = new Client;
        $response = $client->request('POST', 'https://slack.com/api/oauth.v2.access', [
            'query' => [
                'client_id' => config('services.slack.client.id'),
                'client_secret' => config('services.slack.client.secret'),
                'redirect_uri' => config('app.url').'/redirect',
                'single_channel' => true,
                'code' => $request->code
            ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        if (!$contents->ok) {
            // TODO: Maybe throw proper error that is inline with errors above
            redirect()->route('index');
        }
        // Create the User
        $user = User::firstOrNew(['user_id' => $contents->authed_user->id, 'team_id' => $contents->team->id]);
        $user->user_id = $contents->authed_user->id;
        $user->team_id = $contents->team->id;
        $user->access_token = $contents->authed_user->access_token;
        $user->save();
        // Redirect with note
        return redirect()->route('index')->with('success', 'setup');
    }
}
