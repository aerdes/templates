// Bootstrap
require('./bootstrap');

// Easing
require('jquery.easing');

// Theme
require('startbootstrap-stylish-portfolio/js/stylish-portfolio.js');

// Enable link to certain tabs
let url = document.location.toString();
if (url.match('#imprint') || url.match('#oauth') || url.match('#privacy') || url.match('#cookie')  || url.match('#terms')) {
    $('a.nav-link[href="#' + url.split('#')[1] + '"]').tab('show');
    location.hash = '#legal';
}

// Count animation
let a = 0;
$(window).scroll(function() {
    let oTop = $('#statistics').offset().top - window.innerHeight;
    if (a === 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function() {
            let $this = $(this),
                countTo = $this.attr('data-count');
            $({countNum: $this.text()}).animate({
                    countNum: countTo
                },
                {
                    duration: 2000,
                    easing: 'swing',
                    step: function() {
                        $this.text(Math.floor(this.countNum));
                    },
                    complete: function() {
                        $this.text(this.countNum);
                    }

                });
        });
        a = 1;
    }
});
