<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="The app for quickly sending Slack messages using templates">
    <meta name="author" content="Lukas Müller">

    <title>Templates</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('logo/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('logo/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('logo/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('logo/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('logo/safari-pinned-tab.svg') }}" color="#f2be1a">
    <link rel="shortcut icon" href="{{ asset('logo/favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-config" content="{{ asset('logo/browserconfig.xml') }}">
    <meta name="theme-color" content="#2d89ef">
</head>

<body id="page-top">

<!-- Navigation -->
<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
</a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a class="js-scroll-trigger" href="#page-top">Templates</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#page-top">Home</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#about">About</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#statistics">Statistics</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#faq">FAQ</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#legal">Legal</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<header class="masthead d-flex">
    <div class="container text-center my-auto">
        <h1 class="mb-1">Templates</h1>
        <h3 class="mb-5">
            <em>Quickly send Slack messages using templates</em>
        </h3>
        <a class="btn btn-light btn-xl py-2 px-4 mb-3 mx-2" href="https://slack.com/oauth/v2/authorize?client_id={{ config('services.slack.client.id') }}&scope=commands&user_scope=chat:write&redirect_uri={{ config('app.url') }}/redirect">
            <span class="align-middle">Add to</span>
            <img class="align-middle" style="height:3rem;" alt="Slack" src="{{ asset('Slack_RGB.png') }}" />
        </a>
        <a class="btn btn-primary btn-xl js-scroll-trigger mb-3 mx-2" href="#about">
            Find out more
        </a>
        <p class="small">By installing or using Templates you agree to the <a class="js-scroll-trigger" href="#legal">terms and policies</a>.</p>
    </div>
    <div class="overlay"></div>
</header>

<!-- About -->
<section class="content-section bg-light" id="about">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8">
                <video class="w-100" autoplay muted loop>
                    <source src="{{ asset('pizza.mp4') }}" type="video/mp4">
                    Your browser does not support the video tag...
                </video>
            </div>
            <div class="col-lg-4">
                <p class="text-left">
                    Do you frequently need to send the same Slack message to different users? Then use Templates to boost your efficiency!
                    <br><br>
                    Create a new template using the command <code>/templates add example "This is an example template that I need to send out daily to various people."</code> and subsequently send it to someone by typing <code>/templates send example</code>.
                    <br><br>
                    You can send your templates to any user or channel in your workspace. Naturally, you can also edit and delete templates. Use <code>/templates help</code> if you have any questions. More features will be added in the future!
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Stats -->
<section class="content-section bg-primary text-white text-center" id="statistics">
    <div class="container">
        <div class="content-section-heading">
            <h2 class="mb-5">Statistics</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-5 mb-md-0 content-section-heading">
                <h2 class="counter-value" data-count="{{ $user_count = \App\User::count() }}">0</h2>
                <h4>
                    <strong>Users</strong>
                </h4>
                <p class="text-faded mb-0">total number of users</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-md-0 content-section-heading">
                <h2 class="counter-value" data-count="{{ $template_count = \App\Template::count() }}">0</h2>
                <h4>
                    <strong>Templates</strong>
                </h4>
                <p class="text-faded mb-0">total number of templates</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-md-0 content-section-heading">
                <h2 class="counter-value" data-count="{{ $template_count ? round(str_word_count(\App\Template::pluck('text')->implode(' '))/$template_count) : 0 }}">0</h2>
                <h4>
                    <strong>Words</strong>
                </h4>
                <p class="text-faded mb-0">average number of words per templates</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-md-0 content-section-heading">
                <h2 class="counter-value" data-count="{{ $workspace_count = \DB::table('users')->groupBy('team_id')->pluck('team_id')->count() }}">0</h2>
                <h4>
                    <strong>Workspaces</strong>
                </h4>
                <p class="text-faded mb-0">total number of workspaces</p>
            </div>
        </div>
    </div>
</section>

<!-- Callout -->
<section class="callout">
    <div class="container text-center">
        <h2 class="mx-auto mb-5">Convinced? </h2>
        <a class="btn btn-light btn-xl py-2 px-4 mb-3 mx-2" href="https://slack.com/oauth/v2/authorize?client_id={{ config('services.slack.client.id') }}&scope=commands&user_scope=chat:write&redirect_uri={{ config('app.url') }}/redirect">
            <span class="align-middle">Add Templates to</span>
            <img class="align-middle" style="height:3rem;" alt="Slack" src="{{ asset('Slack_RGB.png') }}" />
        </a>
        <a class="btn btn-primary btn-xl js-scroll-trigger mb-3 mx-2" href="#faq">
            Nah, I still have questions
        </a>
        <p class="small">By installing or using Templates you agree to the <a class="js-scroll-trigger" href="#legal">terms and policies</a>.</p>
    </div>
</section>

<!-- FAQ -->
<section class="content-section" id="faq">
    <div class="container">
        <div class="content-section-heading text-center">
            <h2 class="mb-5">FAQ</h2>
        </div>
        <ul class="list-group list-group-flush">
            <!-- TODO: Find out why border-top-0 is needed here at all -->
            <li class="list-group-item border-top-0">
                <h5>How much does this app cost?</h5>
                Nothing. It is free of charge.
            </li>
            <li class="list-group-item">
                <h5>Is this app really free?</h5>
                Sure! It is even open-source and everyone can contribute and add new features. Head over to the <a href="https://bitbucket.org/aerdes/templates/">repository</a> if you are interested in the code. Also note that this app is undergoing continuous development. You can expect some additional features and more graphic interfaces to be added in the future.
            </li>
            <li class="list-group-item">
                <h5>Do you have a manual for this app?</h5>
                Not really. Take a look at the example above and/or use the command <code>/templates help</code> directly within Slack to receive some guidance. Don't worry because this app is really easy to use.
            </li>
            <li class="list-group-item">
                <h5>What to do if the app does not recognize my commands?</h5>
                Make sure to only use straight quotation marks to escape your text (thus use <code>/templates add example "text"</code> but not <code>/templates add example “text”</code>). You can also try to access the help section with the command <code>/templates help</code>.
            </li>
            <li class="list-group-item">
                <h5>What data do you store?</h5>
                We are storing only minimal amounts of data. This includes your Slack user and team identification numbers as well as the templates that you create. Additionally we need to store a token from Slack in order to actually send the templates. Please note that we encrypt this token prior to saving it in the database for security reasons. You can always delete your templates using the command <code>/templates delete</code>. They will be gone forever.
            </li>
            <li class="list-group-item">
                <h5>How can I give feedback?</h5>
                Please send it to <a href="mailto:templates@aerdes.com">templates@aerdes.com</a>. We will probably appreciate it &#128521;
            </li>
        </ul>
    </div>
</section>

<!-- Call to Action -->
<section id="legal" class="content-section bg-primary text-white">

    <div class="container text-center">

        <ul class="nav nav-pills justify-content-center">
            <li class="nav-item mx-2 mb-2">
                <a class="btn btn-light btn-lg nav-link" data-toggle="pill" href="#imprint">Imprint</a>
            </li>
            <li class="nav-item mx-2 mb-2">
                <a class="btn btn-light btn-lg nav-link" data-toggle="pill" href="#scopes">OAuth Scopes</a>
            </li>
            <li class="nav-item mx-2 mb-2">
                <a class="btn btn-light btn-lg nav-link" data-toggle="pill" href="#privacy">Privacy Policy</a>
            </li>
            <li class="nav-item mx-2 mb-2">
                <a class="btn btn-light btn-lg nav-link" data-toggle="pill" href="#cookie">Cookie Policy</a>
            </li>
            <li class="nav-item mx-2 mb-2">
                <a class="btn btn-light btn-lg nav-link" data-toggle="pill" href="#terms">Terms of Service</a>
            </li>
        </ul>

        <div class="collapse fade mt-5" id="imprint">
            <p class="text-left mb-0">
                Lukas Müller<br>
                Schulstraße 1<br>
                56412 Heilberscheid<br>
                Germany<br><br>
                <a class="text-light" href="mailto:templates@aerdes.com">templates@aerdes.com</a><br>
                <a class="text-light" href="tel:+4917696153957">+49 176 96153957</a><br><br>
                <b>Disclaimer</b><br>
                This disclaimer ("Disclaimer", "Agreement") is an agreement between Website Operator ("Website Operator", "us", "we" or "our") and you ("User", "you" or "your"). This Disclaimer sets forth the general guidelines, terms and conditions of your use of the templates.aerdes.com website and any of its products or services (collectively, "Website" or "Services").<br>
                <br>
                <b>Representation</b><br>
                Any views or opinions represented in this Website belong solely to the Content creators and do not represent those of people, institutions or organizations that the Website Operator or creators may or may not be associated with in professional or personal capacity, unless explicitly stated. Any views or opinions are not intended to malign any religion, ethnic group, club, organization, company, or individual.<br>
                <br>
                <b>Content and postings</b><br>
                You may print a copy of any part of this Website for your personal or non-commercial use.Compensation<br>
                Some of the links on the Website may be "affiliate links". This means if you click on the link and purchase an item, Website Operator will receive an affiliate commission.Indemnification and warranties<br>
                While we have made every attempt to ensure that the information contained on the Website is correct, Website Operator is not responsible for any errors or omissions, or for the results obtained from the use of this information. All information on the Website is provided "as is", with no guarantee of completeness, accuracy, timeliness or of the results obtained from the use of this information, and without warranty of any kind, express or implied. In no event will Website Operator, or its partners, employees or agents, be liable to you or anyone else for any decision made or action taken in reliance on the information on the Website or for any consequential, special or similar damages, even if advised of the possibility of such damages. Information on the Website is for general information purposes only and is not intended to provide legal, financial, medical, or any other type of professional advice. Please seek professional assistance should you require it. Furthermore, information contained on the Website and any pages linked to and from it are subject to change at any time and without warning.<br>
                We reserve the right to modify this Disclaimer relating to the Website or Services at any time, effective upon posting of an updated version of this Disclaimer on the Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Website after any such changes shall constitute your consent to such changes. Policy was created with https://www.WebsitePolicies.com<br>
                <br>
                <b>Acceptance of this disclaimer</b><br>
                You acknowledge that you have read this Disclaimer and agree to all its terms and conditions. By accessing the Website you agree to be bound by this Disclaimer. If you do not agree to abide by the terms of this Disclaimer, you are not authorized to use or access the Website.<br>
                <br>
                <b>Contacting us</b><br>
                If you have any questions about this Disclaimer, please contact us.<br>
                <br>
                This document was last updated on July 28, 2019
            </p>
        </div>
        <div class="collapse fade mt-5" id="scopes">
            <p class="text-left mb-0">
                The following OAuth Scopes are the permissions that the Templates app requires from Slack to function properly. <br><br>
                <b>Add slash commands and add actions to messages (and view related content)</b><br>
                This scope is needed to implement the <code>/template</code> command.<br><br>
                <b>Send messages as you</b><br>
                This scope is needed to actually send the templates in your name.
            </p>
        </div>
        <div class="collapse fade mt-5" id="privacy">
            <p class="text-left mb-0">
                This privacy policy ("Policy") describes how the Application/Website Developer ("Application/Website Developer", "we", "us" or "our") collects, protects and uses the personally identifiable information ("Personal Information") you ("User", "you" or "your") may provide in the Templates application/website and any of its products or services (collectively, "Application/Website" or "Services"). It also describes the choices available to you regarding our use of your Personal Information and how you can access and update this information. This Policy does not apply to the practices of companies that we do not own or control, or to individuals that we do not employ or manage.<br>
                <br>
                <b>Collection of personal information</b><br>
                    We receive and store any information you knowingly provide to us when you create an account, publish content, fill any online forms in the Application/Website.  When required this information may include your email address, name,  or other Personal Information. You can choose not to provide us with certain information, but then you may not be able to take advantage of some of the Application/Website's features. Users who are uncertain about what information is mandatory are welcome to contact us.<br>
                 <br>
                 <b>Collection of non-personal information</b><br>
                When you open the Application/Website our servers automatically record information that your device sends. This data may include information such as your device's IP address and location, device name and version, operating system type and version, language preferences, information you search for in our Application/Website, access times and dates, and other statistics.<br>
                <br>
                <b>Use and processing of collected information</b><br>
                Any of the information we collect from you may be used to personalize your experience; improve our Application/Website; improve customer service and respond to queries and emails of our customers; send notification emails such as password reminders, updates, etc;  run and operate our Application/Website and Services. Non-Personal Information collected is used only to identify potential cases of abuse and establish statistical information regarding Application/Website traffic and usage. This statistical information is not otherwise aggregated in such a way that would identify any particular user of the system.<br>
                We may process Personal Information related to you if one of the following applies: (i) You have given your consent for one or more specific purposes. Note that under some legislations we may be allowed to process information until you object to such processing (by opting out), without having to rely on consent or any other of the following legal bases below. This, however, does not apply, whenever the processing of Personal Information is subject to European data protection law; (ii) Provision of information is necessary for the performance of an agreement with you and/or for any pre-contractual obligations thereof; (iii) Processing is necessary for compliance with a legal obligation to which you are subject; (iv) Processing is related to a task that is carried out in the public interest or in the exercise of official authority vested in us; (v) Processing is necessary for the purposes of the legitimate interests pursued by us or by a third party. In any case, we will be happy to clarify the specific legal basis that applies to the processing, and in particular whether the provision of Personal Data is a statutory or contractual requirement, or a requirement necessary to enter into a contract.<br>
                <br>
                <b>Information transfer and storage</b><br>
                Depending on your location, data transfers may involve transferring and storing your information in a country other than your own. You are entitled to learn about the legal basis of information transfers to a country outside the European Union or to any international organization governed by public international law or set up by two or more countries, such as the UN, and about the security measures taken by us to safeguard your information. If any such transfer takes place, you can find out more by checking the relevant sections of this document or inquire with us using the information provided in the contact section.<br>
                <br>
                <b>The rights of users</b><br>
                You may exercise certain rights regarding your information processed by us. In particular, you have the right to do the following: (i) you have the right to withdraw consent where you have previously given your consent to the processing of your information; (ii) you have the right to object to the processing of your information if the processing is carried out on a legal basis other than consent; (iii) you have the right to learn if information is being processed by us, obtain disclosure regarding certain aspects of the processing and obtain a copy of the information undergoing processing; (iv) you have the right to verify the accuracy of your information and ask for it to be updated or corrected; (v) you have the right, under certain circumstances, to restrict the processing of your information, in which case, we will not process your information for any purpose other than storing it; (vi) you have the right, under certain circumstances, to obtain the erasure of your Personal Information from us; (vii) you have the right to receive your information in a structured, commonly used and machine readable format and, if technically feasible, to have it transmitted to another controller without any hindrance. This provision is applicable provided that your information is processed by automated means and that the processing is based on your consent, on a contract which you are part of or on pre-contractual obligations thereof.<br>
                <br>
                <b>The right to object to processing</b><br>
                Where Personal Information is processed for the public interest, in the exercise of an official authority vested in us or for the purposes of the legitimate interests pursued by us, you may object to such processing by providing a ground related to your particular situation to justify the objection. You must know that, however, should your Personal Information be processed for direct marketing purposes, you can object to that processing at any time without providing any justification. To learn, whether we are processing Personal Information for direct marketing purposes, you may refer to the relevant sections of this document.<br>
                <br>
                <b>How to exercise these rights</b><br>
                Any requests to exercise User rights can be directed to the Owner through the contact details provided in this document. These requests can be exercised free of charge and will be addressed by the Owner as early as possible and always within one month.<br>
                <br>
                <b>Privacy of children</b><br>
                We do not knowingly collect any Personal Information from children under the age of 13. If you are under the age of 13, please do not submit any Personal Information through our Application/Website or Service. We encourage parents and legal guardians to monitor their children's Internet usage and to help enforce this Policy by instructing their children never to provide Personal Information through our Application/Website or Service without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Information to us through our Application/Website or Service, please contact us. You must also be at least 16 years of age to consent to the processing of your personal data in your country (in some countries we may allow your parent or guardian to do so on your behalf).<br>
                <br>
                <b>Affiliates</b><br>
                We may disclose information about you to our affiliates for the purpose of being able to offer you related or additional products and services. Any information relating to you that we provide to our affiliates will be treated by those affiliates in accordance with the terms of this Privacy Policy.<br>
                <br>
                <b>Links to other mobile applications</b><br>
                Our Application/Website contains links to other mobile applications that are not owned or controlled by us. Please be aware that we are not responsible for the privacy practices of such other mobile applications or third-parties. We encourage you to be aware when you leave our Application/Website and to read the privacy statements of each and every mobile application that may collect Personal Information.<br>
                <br>
                <b>Information security</b><br>
                We secure information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use, or disclosure. We maintain reasonable administrative, technical, and physical safeguards in an effort to protect against unauthorized access, use, modification, and disclosure of Personal Information in its control and custody. However, no data transmission over the Internet or wireless network can be guaranteed. Therefore, while we strive to protect your Personal Information, you acknowledge that (i) there are security and privacy limitations of the Internet which are beyond our control; (ii) the security, integrity, and privacy of any and all information and data exchanged between you and our Application/Website cannot be guaranteed; and (iii) any such information and data may be viewed or tampered with in transit by a third-party, despite best efforts.<br>
                <br>
                <b>Data breach</b><br>
                In the event we become aware that the security of the Application/Website has been compromised or users Personal Information has been disclosed to unrelated third parties as a result of external activity, including, but not limited to, security attacks or fraud, we reserve the right to take reasonably appropriate measures, including, but not limited to, investigation and reporting, as well as notification to and cooperation with law enforcement authorities. In the event of a data breach, we will make reasonable efforts to notify affected individuals if we believe that there is a reasonable risk of harm to the user as a result of the breach or if notice is otherwise required by law. When we do, we will post a notice in the Application/Website.<br>
                <br>
                <b>Legal disclosure</b><br>
                We will disclose any information we collect, use or receive if required or permitted by law, such as to comply with a subpoena, or similar legal process, and when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request. In the event we go through a business transition, such as a merger or acquisition by another company, or sale of all or a portion of its assets, your user account, and personal data will likely be among the assets transferred.<br>
                <br>
                <b>Changes and amendments</b><br>
                We reserve the right to modify this Policy relating to the Application/Website or Services at any time, effective upon posting of an updated version of this Policy in the Application/Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Application/Website after any such changes shall constitute your consent to such changes. Policy was created with https://www.WebsitePolicies.com<br>
                <br>
                <b>Acceptance of this policy</b><br>
                You acknowledge that you have read this Policy and agree to all its terms and conditions. By using the Application/Website or its Services you agree to be bound by this Policy. If you do not agree to abide by the terms of this Policy, you are not authorized to use or access the Application/Website and its Services.<br>
                <br>
                <b>Contacting us</b><br>
                If you have any questions about this Policy, please contact us.<br>
                <br>
                This document was last updated on July 28, 2019
            </p>
        </div>
        <div class="collapse fade mt-5" id="cookie">
            <p class="text-left mb-0">
                This cookie policy ("Policy") describes what cookies are and how Website Operator ("Website Operator", "we", "us" or "our") uses them on the templates.aerdes.com website and any of its products or services (collectively, "Website" or "Services").<br>
                <br>
                You should read this Policy so you can understand what type of cookies we use, the information we collect using cookies and how that information is used. It also describes the choices available to you regarding accepting or declining the use of cookies. For further information on how we use, store and keep your personal data secure, see our Privacy Policy.<br>
                <br>
                <b>What are cookies?</b><br>
                Cookies are small pieces of data stored in text files that are saved on your computer or other devices when websites are loaded in a browser. They are widely used to remember you and your preferences, either for a single visit (through a "session cookie") or for multiple repeat visits (using a "persistent cookie").<br>
                Session cookies are temporary cookies that are used during the course of your visit to the Website, and they expire when you close the web browser.<br>
                Persistent cookies are used to remember your preferences within our Website and remain on your desktop or mobile device even after you close your browser or restart your computer. They ensure a consistent and efficient experience for you while visiting our Website or using our Services.<br>
                Cookies may be set by the Website ("first-party cookies"), or by third parties, such as those who serve content or provide advertising or analytics services on the website ("third party cookies"). These third parties can recognize you when you visit our website and also when you visit certain other websites.<br>
                <br>
                <b>What type of cookies do we use?</b><br>
                - Necessary cookies: Necessary cookies allow us to offer you the best possible experience when accessing and navigating through our Website and using its features. For example, these cookies let us recognize that you have created an account and have logged into that account to access the content.<br>
                - Functionality cookies: Functionality cookies let us operate the Website and our Services in accordance with the choices you make. For example, we will recognize your username and remember how you customized the Website and Services during future visits.<br>
                - Analytical cookies: These cookies enable us and third-party services to collect aggregated data for statistical purposes on how our visitors use the Website. These cookies do not contain personal information such as names and email addresses and are used to help us improve your user experience of the Website.<br>
                <br>
                <b>What are your cookie options?</b><br>
                If you don't like the idea of cookies or certain types of cookies, you can change your browser's settings to delete cookies that have already been set and to not accept new cookies. To learn more about how to do this or to learn more about cookies, visit internetcookies.org<br>
                Please note, however, that if you delete cookies or do not accept them, you might not be able to use all of the features our Website and Services offer.<br>
                <br>
                <b>Changes and amendments</b><br>
                We reserve the right to modify this Policy relating to the Website or Services at any time, effective upon posting of an updated version of this Policy on the Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Website after any such changes shall constitute your consent to such changes. Policy was created with https://www.WebsitePolicies.com<br>
                <br>
                <b>Acceptance of this policy</b><br>
                You acknowledge that you have read this Policy and agree to all its terms and conditions. By using the Website or its Services you agree to be bound by this Policy. If you do not agree to abide by the terms of this Policy, you are not authorized to use or access the Website and its Services.<br>
                <br>
                <b>Contacting us</b><br>
                If you have any questions about this Policy or our use of cookies, please contact us.<br>
                <br>
                This document was last updated on July 28, 2019
            </p>
        </div>
        <div class="collapse fade mt-5" id="terms">
            <p class="text-left mb-0">
                These terms and conditions ("Terms", "Agreement") are an agreement between the Application/Website Developer ("Application/Website Developer", "us", "we" or "our") and you ("User", "you" or "your"). This Agreement sets forth the general terms and conditions of your use of the Templates application/website and any of its products or services (collectively, "Application/Website" or "Services").<br>
                <br>
                <b>Description</b><br>
                Our Application/Website allows you to quickly send Slack messages using templates. These templates need to be registered beforehand in Slack using the appropriate Slash command. These templates are not saved on Slack servers but instead on those of the the Application/Website Developer.<br>
                The Application/Website Developer may also, in the future, update current services and/or features or offer new services and/or features. Such updated or new features and/or services shall be subject to the terms and conditions of this Agreement as well.<br>
                Access to our Application/Website is permitted on a temporary basis and it is free of charge. We reserve the right to modify or discontinue offering or updating our Services at any time without notice.<br>
                <br>
                <b>No association</b><br>
                The Application/Website Developer and Slack Technologies Inc, the provider of the Slack communication service, are different entities. There is no relationship between the Application/Website Developer and Slack, other than the Application/Website Developer being a licensee and user of the Slack API for the purpose of providing our Services. Slack is not responsible for our Application/Website and will not provide support for our Application/Website.<br>
                These terms do not apply to your use of the Slack services. Such use is governed by the Slack Terms of Service available on the Slack website. <br>
                <br>
                <b>Accounts and membership</b><br>
                If you create an account in the Application/Website, you are responsible for maintaining the security of your account and you are fully responsible for all activities that occur under the account and any other actions taken in connection with it. We may, but have no obligation to, monitor and review new accounts before you may sign in and use our Services. Providing false contact information of any kind may result in the termination of your account. You must immediately notify us of any unauthorized uses of your account or any other breaches of security. We will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions. We may suspend, disable, or delete your account (or any part thereof) if we determine that you have violated any provision of this Agreement or that your conduct or content would tend to damage our reputation and goodwill. If we delete your account for the foregoing reasons, you may not re-register for our Services. We may block your email address and Internet protocol address to prevent further registration.<br>
                <br>
                <b>User content</b><br>
                We do not own any data, information or material ("Content") that you submit in the Application/Website in the course of using the Service. You shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all submitted Content. We may, but have no obligation to, monitor and review Content in the Application/Website submitted or created using our Services by you. Unless specifically permitted by you, your use of the Application/Website does not grant us the license to use, reproduce, adapt, modify, publish or distribute the Content created by you or stored in your user account for commercial, marketing or any similar purpose. But you grant us permission to access, copy, distribute, store, transmit, reformat, display and perform the Content of your user account solely as required for the purpose of providing the Services to you. Without limiting any of those representations or warranties, we have the right, though not the obligation, to, in our own sole discretion, refuse or remove any Content that, in our reasonable opinion, violates any of our policies or is in any way harmful or objectionable.<br>
                <br>
                <b>Backups</b><br>
                We perform regular backups of the  Content, however, these backups are for our own administrative purposes only and are in no way guaranteed. You are responsible for maintaining your own backups of your data. We do not provide any sort of compensation for lost or incomplete data in the event that backups do not function properly. We will do our best to ensure complete and accurate backups, but assume no responsibility for this duty.<br>
                <br>
                <b>Links to other mobile applications</b><br>
                Although this Application/Website may link to other mobile applications, we are not, directly or indirectly, implying any approval, association, sponsorship, endorsement, or affiliation with any linked mobile application, unless specifically stated herein. Some of the links in the Application/Website may be "affiliate links". This means if you click on the link and purchase an item, Application/Website Developer will receive an affiliate commission. We are not responsible for examining or evaluating, and we do not warrant the offerings of, any businesses or individuals or the content of their mobile applications. We do not assume any responsibility or liability for the actions, products, services, and content of any other third-parties. You should carefully review the legal statements and other conditions of use of any mobile application which you access through a link from this Application/Website. Your linking to any other off-site mobile applications is at your own risk.<br>
                <br>
                <b>Prohibited uses</b><br>
                In addition to other terms as set forth in the Agreement, you are prohibited from using the Application/Website or its Content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related mobile application, other mobile applications, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related mobile application, other mobile applications, or the Internet. We reserve the right to terminate your use of the Service or any related mobile application for violating any of the prohibited uses.<br>
                <br>
                <b>Limitation of liability</b><br>
                To the fullest extent permitted by applicable law, in no event will Application/Website Developer, its affiliates, officers, directors, employees, agents, suppliers or licensors be liable to any person for (a): any indirect, incidental, special, punitive, cover or consequential damages (including, without limitation, damages for lost profits, revenue, sales, goodwill, use of content, impact on business, business interruption, loss of anticipated savings, loss of business opportunity) however caused, under any theory of liability, including, without limitation, contract, tort, warranty, breach of statutory duty, negligence or otherwise, even if Application/Website Developer has been advised as to the possibility of such damages or could have foreseen such damages. To the maximum extent permitted by applicable law, the aggregate liability of Application/Website Developer and its affiliates, officers, employees, agents, suppliers and licensors, relating to the services will be limited to an amount greater of one dollar or any amounts actually paid in cash by you to Application/Website Developer for the prior one month period prior to the first event or occurrence giving rise to such liability. The limitations and exclusions also apply if this remedy does not fully compensate you for any losses or fails of its essential purpose.<br>
                <br>
                <b>Indemnification</b><br>
                You agree to indemnify and hold Application/Website Developer and its affiliates, directors, officers, employees, and agents harmless from and against any liabilities, losses, damages or costs, including reasonable attorneys' fees, incurred in connection with or arising from any third-party allegations, claims, actions, disputes, or demands asserted against any of them as a result of or relating to your Content, your use of the Application/Website or Services or any willful misconduct on your part.<br>
                <br>
                <b>Severability</b><br>
                All rights and restrictions contained in this Agreement may be exercised and shall be applicable and binding only to the extent that they do not violate any applicable laws and are intended to be limited to the extent necessary so that they will not render this Agreement illegal, invalid or unenforceable. If any provision or portion of any provision of this Agreement shall be held to be illegal, invalid or unenforceable by a court of competent jurisdiction, it is the intention of the parties that the remaining provisions or portions thereof shall constitute their agreement with respect to the subject matter hereof, and all such remaining provisions or portions thereof shall remain in full force and effect.<br>
                <br>
                <b>Changes and amendments</b><br>
                We reserve the right to modify this Agreement or its policies relating to the Application/Website or Services at any time, effective upon posting of an updated version of this Agreement in the Application/Website. When we do, we will revise the updated date at the bottom of this page. Continued use of the Application/Website after any such changes shall constitute your consent to such changes. Policy was created with https://www.WebsitePolicies.com<br>
                <br>
                <b>Acceptance of these terms</b><br>
                You acknowledge that you have read this Agreement and agree to all its terms and conditions. By using the Application/Website or its Services you agree to be bound by this Agreement. If you do not agree to abide by the terms of this Agreement, you are not authorized to use or access the Application/Website and its Services.<br>
                <br>
                <b>Contacting us</b><br>
                If you have any questions about this Agreement, please contact us.<br>
                <br>
                This document was last updated on July 28, 2019
            </p>
        </div>

    </div>
</section>

<!-- Footer -->
<footer class="footer text-center">
    <div class="container">
        <p class="text-muted small mb-0">
            copyright by <a href="https://aerdes.com">aerdes</a>. all rights reserved.
            <br>
            developed in germany using <a href="https://laravel.com/"><i class="fab fa-laravel"></i></a> and <a href="https://bitbucket.org/aerdes/templates/"><i class="fab fa-bitbucket"></i></a> while drinking some <a href="https://en.wikipedia.org/wiki/Beer_in_Germany"><i class="fas fa-beer"></i></a>.
        </p>
    </div>
</footer>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Success modal -->
@if(session('success'))
    <div class="modal fade" id="success" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Success &#128526;</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if(session('success') == 'setup')
                        Thanks for setting up Templates.
                        <br><br>
                        Head over to Slack and get started by typing <code>/templates add start "This is my first template"</code> and subsequently send your template by typing <code>/templates send start</code>.
                    @else
                        {{ session('success') }}
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endif

<!-- JavaScript -->
<script src="{{ mix('js/app.js') }}"></script>
@if(session('success'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#success').modal('show');
        });
    </script>
@endif
</body>

</html>
